# gender neutral parenthood emojis

I made gender neutral and male versions of the breast feeding and pregnancy emojis. Are there other emojis with just one gender represented?
I used the SVG files of the [twemojii](https://github.com/twitter/twemoji) emoji set.

## They look like this:
![pregnant woman ](render_72px/pregnant-woman.png)
![pregnant person](render_72px/pregnant-person.png)
![pregnant man   ](render_72px/pregnant-man.png)

![breast feeding woman ](render_72px/breast-feeding-woman.png)
![breast feeding person](render_72px/breast-feeding-person.png)
![breast feeding man   ](render_72px/breast-feeding-man.png)
